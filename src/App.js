import React from 'react';
import './App.css';
import { Body,Navbar,Notification } from './screens'

function App() {
 
  return (
    <div className="App-main-layout">
          <div className="App-main-layout-left">
          <Navbar/>
          </div>
          <div className="App-main-layout-center">
            <Body/>
         </div> 
         <div className="App-main-layout-right">
            <Notification/>
         </div>
    </div>
  );
}

export default App;
