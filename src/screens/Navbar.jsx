import React, { useState }from 'react';
import './Screen_styles.css';

//Navabar is the Left section
function Navbar() {
  const [showText, setShowText] = useState(false);
  const [showText1, setShowText1] = useState(false);
  const [showText2, setShowText2] = useState(false);
  return (
    <div className="navbar-main-layout">
       <div className="navbar-main-layout-logo">

       </div>
       <div className="navbar-main-layout-content">
         <div className="nav-content">HOME</div> 
         <div className="nav-content" onClick={() => setShowText(!showText)}>ALL CLASSROOMS</div> 
             {showText && 
              <div className='drop-down-layout'>
                  <button className='drop-down-content'>BCA PROJECTS</button>
                  <button className='drop-down-content'>BCA PROJECTS</button>
                  <button className='drop-down-content'>BCA PROJECTS</button>
              </div>}
         <div className="nav-content" onClick={() => setShowText1(!showText1)}>GROUP</div> 
               {showText1 && 
               <div className='drop-down-layout'>
                 <button className='drop-down-content'>BCA PROJECTS</button>
                 <button className='drop-down-content'>BCA PROJECTS</button>
                 <button className='drop-down-content'>BCA PROJECTS</button>
               </div>}
         <div className="nav-content" onClick={() => setShowText2(!showText2)}>CLASSROOMS</div> 
            {showText2 && <div className='drop-down-layout'>
                <button className='drop-down-content'>BCA PROJECTS</button>
                <button className='drop-down-content'>BCA PROJECTS</button>
                <button className='drop-down-content'>BCA PROJECTS</button>
               </div>}
         <div className="nav-content" >SETTINGS</div> 
       </div>           
    </div>
  );
}

export default Navbar;