import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

//this is  model design structure not used in this project

const useStyles = makeStyles((theme) => ({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    paper: {
      backgroundColor: '#282c34',
      marginLeft: "80vw",
      marginTop: "28vw",
      width:'170px',
      height:'190px',
      borderRadius:'10px',
      outline:'none',
      padding: theme.spacing(2, 4, 3)
    }
  }));
  
  export default function TransitionsModal() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
  
    const handleOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };
  
    return (
      <div>
        <button type="button" onClick={handleOpen}>
          Message
        </button>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={open}>
            <div className={classes.paper}>
              <h2 id="transition-modal-title" className='message'>Messages</h2>
              <p id="transition-modal-description" className='message'>
                Hai
              </p>
            </div>
          </Fade>
        </Modal>
      </div>
    );
  }
  
