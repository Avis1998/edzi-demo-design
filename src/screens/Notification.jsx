import React from 'react';
import './Screen_styles.css';
import  Calendar  from '../Calendar/Calendar';

//Fuctional component Notification for right section

function Notification() {
 
  return (
   <div className="notificatio-main-layout">
    <div className="notificatio-main-layout-top">
          
    </div>
    <div className="notification-main-bottom">
        <div className="notification-calender">        
           <div className='calender-style'>
             <div className='calender-style-in'>
                <div className='calender-style-c'>
                   <Calendar/>
                </div> 
                <div className='calender-style-date'>
                   <p style={{fontSize:"9px"}}>10:30 AM</p>
                   <p style={{fontSize:"13px"}}>Java Assignment last date</p>
                   <p style={{fontSize:"9px"}}>10:30 PM</p>
                   <p style={{fontSize:"13px"}}>New Activity Started</p>
               </div>  
            </div>     
          </div> 
        </div>
   </div> 
   
  </div>
  );
}

export default Notification;