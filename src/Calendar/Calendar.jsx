import React from 'react';
import { useState, useEffect } from 'react';
import './CalendarStyle.css';

 function Calendar() {
  const DAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  const DAYS_LEAP = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  const DAYS_OF_THE_WEEK = ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'];
  const MONTHS = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUN', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBAR', 'NOVEMBER', 'DECEMBER'];

  const today = new Date();
  const [date, setDate] = useState(today);
  const [day, setDay] = useState(date.getDate());
  const [month, setMonth] = useState(date.getMonth());
  const [year, setYear] = useState(date.getFullYear());
  const [startDay, setStartDay] = useState(getStartDayOfMonth(date));
  
  useEffect(() => {
    setDay(date.getDate());
    setMonth(date.getMonth());
    setYear(date.getFullYear());
    setStartDay(getStartDayOfMonth(date));
  }, [date]);

  function getStartDayOfMonth(date: Date) {
    return new Date(date.getFullYear(), date.getMonth(), 1).getDay();
  }

  function isLeapYear(year: number) {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  }

  const days = isLeapYear ? DAYS_LEAP : DAYS;
  

  return (
    <div className='Frame'>
    < div className='Header'>
      <div className='header-month-year'>
        {MONTHS[month]} {year}
      </div>
      <button className='header-prev-btn' onClick={() => setDate(new Date(year, month - 1, day))}>‹</button>
      <button className='header-nxt-btn' onClick={() => setDate(new Date(year, month + 1, day))}>›</button>
    </div>
    <div className='Body'>
      {DAYS_OF_THE_WEEK.map( d => ( 
        < div className='Day' 
       
            key={d}>{d} 
        
        </div> ) )  }
      
      {Array(days[month] + (startDay - 1))
        .fill(null)
        .map((_, index) => {
          const d = index - (startDay - 2);
          return (
            <div className='day'
              key={index}
              isToday={d === today.getDate()}
              isSelected={d === day}
              onClick={() => setDate(new Date(year, month, d))}
            >
              {d > 0 ? d : ''}
              </div>
          );
        })}
     </div>
  </div>
  );
}

export default Calendar;